(function(context) {

	function alarm(stop) {
		window.alarmAudioContext = !window.alarmAudioContext ? new(window.AudioContext || window.webkitAudioContext)() : window.alarmAudioContext;
		window.alarmOsc = !window.alarmOsc ? window.alarmAudioContext.createOscillator() : window.alarmOsc;
		window.alarmOsc.type = 'sine'; // this is the default - also square, sawtooth, triangle
		window.alarmOsc.frequency.value = 440; // Hz
		window.alarmOsc.connect(window.alarmAudioContext.destination); // connect it to the destination
		try {
			window.alarmOsc.start();
		} catch (e) {
			if (window.alarmAudioContext.state == 'running') {
				window.alarmAudioContext.suspend();
			} else {
				window.alarmAudioContext.resume();
			}
		}
		if (stop) {
			window.alarmAudioContext.suspend();
		}
	}

	context.utils = {
		alarm: alarm
	}

})(window)