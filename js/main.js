(function() {
	var canvas = document.getElementById("base-canvas"),
		context = canvas.getContext("2d"),
		img = document.createElement("img"),
		mouseDown = false,
		brushColor = "rgb(0, 0, 0)",
		hasText = true,
		baseImage = document.getElementById('defaultImg'),
		clearCanvas = function() {
			if (hasText) {
				context.clearRect(0, 0, canvas.width, canvas.height);
				hasText = false;
			}
		};

	canvas.loadedImage = false;
	context.canvas.width = baseImage.width;
	context.canvas.height = baseImage.height;
	context.drawImage(baseImage, 0, 0);

	// Adding instructions
	context.fillText("Drop an image onto the canvas", 100, 150);

	// Image for loading	
	img.addEventListener("load", function() {
		clearCanvas();
		// Resize canvas with target image dimensions
		context.canvas.width = img.width;
		context.canvas.height = img.height;
		context.drawImage(img, 0, 0);
	}, false);

	// To enable drag and drop
	canvas.addEventListener("dragover", function(evt) {
		evt.preventDefault();
	}, false);

	// Handle dropped image file - only Firefox and Google Chrome
	canvas.addEventListener("drop", function(evt) {
		var files = evt.dataTransfer.files;
		if (files.length > 0) {
			var file = files[0];
			if (typeof FileReader !== "undefined" && file.type.indexOf("image") != -1) {
				canvas.loadedImage = true;
				var reader = new FileReader();
				// Note: addEventListener doesn't work in Google Chrome for this event
				reader.onload = function(evt) {
					img.src = evt.target.result;
				};
				reader.readAsDataURL(file);
			}
		} else {
			if (evt.dataTransfer.types.indexOf('text/uri-list') >= 0) {
				img.src = evt.dataTransfer.getData('text/uri-list');
				canvas.loadedImage = true;
			}
		}
		evt.preventDefault();
	}, false);

	var resultCanvas = document.getElementById("result-canvas");
	var baseCanvas = document.getElementById("base-canvas");

	window.ann = new NN(baseCanvas, resultCanvas);
	var commands = document.getElementById("commands");

	// Save image
	var saveImage = document.createElement("button");
	saveImage.innerHTML = "Save canvas";
	saveImage.addEventListener("click", function(evt) {
		window.open(resultCanvas.toDataURL("image/png"));
		evt.preventDefault();
	}, false);

	// Start ANN
	var start = document.createElement("button");
	start.innerHTML = "Start";
	start.addEventListener("click", function(evt) {
		window.ann.start();
		evt.preventDefault();
	}, false);

	// pauseContinue ANN
	var pauseContinue = document.createElement("button");
	pauseContinue.innerHTML = "Pause/Continue";
	pauseContinue.addEventListener("click", function(evt) {
		window.ann.pauseContinue();
		evt.preventDefault();
	}, false);


	commands.appendChild(start);
	commands.appendChild(pauseContinue);
	commands.appendChild(saveImage);

})();