/*
 As camadas da rede podem ser configuradas na função newPerceptron. 
 O número de entradas e saídas não deve ser alterado. 
 Exemplo de configurações válidas: (2, 15, 3), (2, 13, 4, 6, 3) etc.
 A arquitetura utilizada é de uma rede Feed-forward.
 
 A taxa de aprendizagem padrão é dinâmica e pode ser alterada na função learningRate.

*/


(function(){
function NN(baseCanvas, resultCanvas) {

	function nnPainter(){
	 	var perceptron = null, 
		 	worker = null, 
		 	index = 0,
		 	baseData = null,
	 		canvas = resultCanvas, 
	 		context = resultCanvas.getContext('2d'),
	 		height = baseCanvas.height,
	 		width = baseCanvas.width;

	 		context.canvas.width = width;
			context.canvas.height = height;
	 	
	 	var	run = false,
	 		iterationPID = null, previewPID = null,

	 		size = height * width,
	 		iteration = 0,
	 		lastIteration = 0,
	 		to = null,
	 		px = null,

	 		lastMSE = 0,
        	tlastOp = performance.now(),
        	tlastMSE = performance.now(),
        	maxMSE = 3*255*255,
        	deltaMSE = 0,

        	startTime = performance.now(),
        	timeElapsed = 0,

	 		trainingStarted = false,

	 		maxTime = 600,
	 		alarmPID = 0;

	 		iterationPixelAccessArray = [];



 		function learningRate(iteration){

	 		// fixed rate
	 		// return .01;

			// Dynamic rate
	 		// return .01 / ((1 + .0005 * iteration));

	 		// Dynamic rate with MSE interaction
	 		return .01 / ((1 + .0005 * iteration) - (lastMSE/(maxMSE)));
	 	}

	 	function newPerceptron(){
			perceptron = new synaptic.Architect.Perceptron(2, 15, 3);
	 	}

	 	function genPixelAccessArray(doShuffle){
	 		var arr = [];

	 		for (var x = 0; x < width; x += 1) {
	 			for (var y = 0; y < height; y += 1) {
	 				arr.push([x, y]);
	 			}
	 		}

	 		let doS = !!doShuffle ? shuffle(arr) : false;
	 		iterationPixelAccessArray = arr;
	 	}

	 	// Fisher-Yates shuffle
	 	function shuffle (array) {
		  var i = 0
		    , j = 0
		    , temp = null

		  for (i = array.length - 1; i > 0; i -= 1) {
		    j = Math.floor(Math.random() * (i + 1))
		    temp = array[i]
		    array[i] = array[j]
		    array[j] = temp
		  }
		}

	 	function getBaseData() {
	 		var baseImage = baseCanvas.loadedImage == true ? baseCanvas : document.getElementById('defaultImg');
	 		context.drawImage(baseImage, 0, 0);
	 		var imageData = context.getImageData(0, 0, width, height);
	 		return imageData.data;
	 	}

	 	function train() {

	 		trainingStarted = true;
	 		run = true;

	 		iteration = 0;

	 		genPixelAccessArray();
	 		
	 		clearInterval(iterationPID);
	 		clearInterval(previewPID);

	 		newPerceptron();
	 		baseData = getBaseData();
	 		preview();

	 		// Try to saturate the interval loop with more iterate calls.
	 		// for and while loops tend to perform worst...
	 		iterationPID = setInterval(function(){ 
	 			iterate(); iterate(); iterate(); iterate(); iterate(); iterate();
	 			iterate(); iterate(); iterate(); iterate(); iterate(); iterate();
	 			iterate(); iterate(); iterate(); iterate(); iterate(); iterate();
	 			iterate(); iterate();
	 			genPixelAccessArray(false);
	 		},0);
	 		previewPID = setInterval(function(){ preview() }, 200);
	 	}

	 	function iterate() {

	 		if(!run){
	 			return;
	 		}

			let x, y;
	 		for (var i = iterationPixelAccessArray.length - 1; i >= 0; i--) {
	 			x = iterationPixelAccessArray[i][0];
	 			y = iterationPixelAccessArray[i][1];
	 			perceptron.activate([x / width, y / height]);
	 			perceptron.propagate(learningRate(iteration), pixel(baseData, x, y));
	 		}

	 		// for (var x = 0; x < width; x += 1) {
	 		// 	for (var y = 0; y < height; y += 1) {
	 		// 		perceptron.activate([x / width, y / height]);
	 		// 		perceptron.propagate(learningRate(iteration), pixel(baseData, x, y));
	 		// 	}
	 		// }

	 		iteration++;
	 	}

	 	function pixel(data, x, y) {

	 		var red = data[((width * y) + x) * 4];
	 		var green = data[((width * y) + x) * 4 + 1];
	 		var blue = data[((width * y) + x) * 4 + 2];

	 		return [red / 255, green / 255, blue / 255];
	 	}

        function measureOps(){
            var tnow = performance.now(); 
            $('#iterations').text(iteration + ' | ' + Math.round((iteration-lastIteration)*1000/(tnow - tlastOp)) + 'iter/s');  
            tlastOp = tnow;
            lastIteration = iteration;
        }

	 	function preview(time) {

	 		if(!run){
	 			return;
	 		}

            measureOps();

	 		var imageData = context.getImageData(0, 0, width, height);
            var errorTable = [];
	 		for (var x = 0; x < width; x++) {
	 			for (var y = 0; y < height; y++) {
	 				var rgb = perceptron.activate([x / width, y / height]);
                    errorTable.push(((rgb[0]) * 255)-baseData[((width * y) + x) * 4]);
                    errorTable.push(((rgb[1]) * 255)-baseData[((width * y) + x) * 4 + 1]);
                    errorTable.push(((rgb[2]) * 255)-baseData[((width * y) + x) * 4 + 2]);
	 				imageData.data[((width * y) + x) * 4] = (rgb[0]) * 255;
	 				imageData.data[((width * y) + x) * 4 + 1] = (rgb[1]) * 255;
	 				imageData.data[((width * y) + x) * 4 + 2] = (rgb[2]) * 255;
	 			}
	 		}
	 		context.putImageData(imageData, 0, 0);

            var tnow = performance.now(); 
            
            var mse = errorTable.reduce(function(i, ii){ return i + (ii*ii);})/errorTable.length;
            deltaMSE = (mse-lastMSE)*1000/(tnow - tlastMSE);
            $('#nnError').text(Math.round(mse) + ' (' + (mse/maxMSE)*100 + '%) | ' + deltaMSE  + 'mse/s');
            lastMSE = mse;

            timeElapsed = Math.round((tnow-startTime)/1000);
            $('#timeElapsed').text(timeElapsed + 's');

            if(timeElapsed >= maxTime){
            	run = false;
            	alarmPID = setInterval(window.utils.alarm, 300);

            }

	 	}

	 	function init(){
	 		train();
	 	}

	 	function stop(){
	 		clearInterval(iterationPID);
	 		clearInterval(previewPID);
	 	}

	 	function stopAlarm(){
	 		clearInterval(alarmPID);
	 		window.utils.alarm(true);
	 	}

	 	function toggleRun(){
	 		if(trainingStarted){
	 			run = !run;
	 		}
	 	}

	 	return {
 				iterate: iterate,
 				preview: preview,
 				pixel: pixel,
 				train: train,
 				getBaseData: getBaseData,
 				init: init,
 				stop: stop,
 				toggleRun: toggleRun,
 				stopAlarm: stopAlarm
 			};
	}

 	this.nn = new nnPainter();
 	var _t = this;

 	function start(){
 		_t.nn.stop();
 		_t.nn = new nnPainter();
 		_t.nn.init();
 	}

 	function pauseContinue(){
 		_t.nn.toggleRun();
 	}

 	function stopAlarm(){
 		_t.nn.stopAlarm();
 	}

 	return {
 		start: start,
 		pauseContinue: pauseContinue,
 		stopAlarm: stopAlarm
 	}
 };
 window.NN = NN;
})()